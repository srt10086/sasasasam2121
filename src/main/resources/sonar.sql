CREATE TABLE public.gov_sonar_metrics
(
    cname                         varchar(10),
    caname_author                 varchar(10),
    generatedLines                varchar(10),
    ncloc                         varchar(10),
    newLines                      varchar(20),
    nclocLanguageDistribution     varchar(100),
    generatedNcloc                varchar(10),
    classes                       varchar(10),
    files                         varchar(10),
    directories                   varchar(6),
    functions                     varchar(10),
    statements                    varchar(10),
    publicApi                     varchar(10),
    projects                      varchar(10),
    commentLines                  varchar(10),
    commentLinesDensity           varchar(6),
    publicDocumentedApiDensity    varchar(6),
    publicUndocumentedApi         varchar(10),
    commentedOutCodeLines         varchar(10),
    complexity                    varchar(10),
    fileComplexity                varchar(100),
    complexityInClasses           varchar(10),
    classComplexity               varchar(100),
    complexityInFunctions         varchar(10),
    functionComplexity            varchar(100),
    classComplexityDistribution   varchar(100),
    functionComplexityDistributio varchar(100),
    fileComplexityDistribution    varchar(100),
    cognitiveComplexity           varchar(10),
    tests                         varchar(10),
    testExecutionTime             varchar(6),
    testErrors                    varchar(10),
    skippedTests                  varchar(10),
    testFailures                  varchar(10),
    testSuccessDensity            varchar(6),
    testData                      varchar(100),
    Coverage                      varchar(6),
    newCoverage                   varchar(6),
    linesToCover                  varchar(10),
    newLinesToCover               varchar(10),
    uncoveredLines                varchar(10),
    newUncoveredLines             varchar(10),
    lineCoverage                  varchar(6),
    newLineCoverage               varchar(6),
    coverageLineHitsData          varchar(100),
    conditionsToCover             varchar(10),
    newConditionsToCover          varchar(10),
    uncoveredConditions           varchar(10),
    newUncoveredConditions        varchar(10),
    branchCoverage                varchar(6),
    newBranchCoverage             varchar(6),
    conditionsByLine              varchar(100),
    coveredConditionsByLine       varchar(100),
    itCoverage                    varchar(6),
    newItCoverage                 varchar(6),
    itLinesToCover                varchar(10),
    newItLinesToCover             varchar(10),
    itUncoveredLines              varchar(10),
    newItUncoveredLines           varchar(10),
    itLineCoverage                varchar(6),
    newItLineCoverage             varchar(6),
    itCoverageLineHitsData        varchar(100),
    itConditionsToCover           varchar(10),
    newItConditionsToCover        varchar(10),
    itUncoveredConditions         varchar(10),
    newItUncoveredConditions      varchar(10),
    itBranchCoverage              varchar(6),
    newItBranchCoverage           varchar(6),
    itConditionsByLine            varchar(100),
    itCoveredConditionsByLine     varchar(100),
    overallCoverage               varchar(6),
    newOverallCoverage            varchar(6),
    overallLinesToCover           varchar(10),
    newOverallLinesToCover        varchar(10),
    overallUncoveredLines         varchar(10),
    newOverallUncoveredLines      varchar(10),
    overallLineCoverage           varchar(6),
    newOverallLineCoverage        varchar(6),
    overallCoverageLineHitsData   varchar(100),
    overallConditionsToCover      varchar(10),
    newOverallConditionsToCover   varchar(10),
    overallUncoveredConditions    varchar(10),
    newOverallUncoveredCondition  varchar(10),
    overallBranchCoverage         varchar(6),
    newOverallBranchCoverage      varchar(6),
    overallConditionsByLine       varchar(100),
    overallCoveredConditionsByL   varchar(100),
    duplicatedLines               varchar(10),
    newDuplicatedLines            varchar(10),
    duplicatedBlocks              varchar(10),
    newDuplicatedBlocks           varchar(10),
    duplicatedFiles               varchar(10),
    duplicatedLinesDensity        varchar(6),
    newDuplicatedLinesDensity     varchar(6),
    duplicationsData              varchar(100),
    Violations                    varchar(10),
    blockerViolations             varchar(10),
    criticalViolations            varchar(10),
    majorViolations               varchar(10),
    minorViolations               varchar(10),
    infoViolations                varchar(10),
    newViolations                 varchar(10),
    newBlockerViolations          varchar(10),
    newCriticalViolations         varchar(10),
    newMajorViolations            varchar(10),
    newMinorViolations            varchar(10),
    newInfoViolations             varchar(10),
    falsePositiveIssues           varchar(10),
    wontFixIssues                 varchar(10),
    openIssues                    varchar(10),
    reopenedIssues                varchar(10),
    confirmedIssues               varchar(10),
    codeSmells                    varchar(10),
    newCodeSmells                 varchar(10),
    bugs                          varchar(10),
    newBugs                       varchar(10),
    Vulnerabilities               varchar(10),
    newVulnerabilities            varchar(10),
    sqaleIndex                    varchar(100),
    newTechnicalDebt              varchar(100),
    sqaleRating                   varchar(100),
    newMaintainabilityRating      varchar(100),
    developmentCost               varchar(100),
    newDevelopmentCost            varchar(100),
    sqaleDebtRatio                varchar(6),
    newSqaleDebtRatio             varchar(6),
    effortToReachMaintainability  varchar(100),
    reliabilityRemediationEffort  varchar(100),
    newReliabilityRemediationEff  varchar(100),
    reliabilityRating             varchar(100),
    newReliabilityRating          varchar(100),
    securityRemediationEffort     varchar(100),
    newSecurityRemediationEffort  varchar(100),
    securityRating                varchar(100),
    newSecurityRating             varchar(100),
    nclocData                     varchar(100),
    commentLinesData              varchar(100),
    executableLinesData           varchar(100),
    alertStatus                   LEVEL(100),
    qualityGateDetails            varchar(100),
    qualityProfiles               varchar(100),
    lastCommitDate                varchar(6),
    burnedBudget                  varchar(100),
    businessValue                 varchar(100),
    teamSize                      varchar(10),
    sonarjavaFeedback             varchar(100)
);

COMMENT
ON TABLE public.gov_sonar_metrics IS 'sonar metrics表';


COMMENT
ON COLUMN public.gov_sonar_metrics.cname                                                 IS '组件名称  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.lines                                                 IS '组件作者  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.lines                                                 IS 'Lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.generatedLines                                        IS 'Number of generated lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.ncloc                                                 IS 'Non commenting lines of code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newLines                                              IS 'New lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.nclocLanguageDistribution                             IS 'Non Commenting Lines of Code Distributed By Language  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.generatedNcloc                                        IS 'Generated non Commenting Lines of Code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.classes                                               IS 'Classes  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.files                                                 IS 'Number of files  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.directories                                           IS 'Directories  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.functions                                             IS 'Functions  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.statements                                            IS 'Number of statements  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.publicApi                                             IS 'Public API  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.projects                                              IS 'Number of projects  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.commentLines                                          IS 'Number of comment lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.commentLinesDensity                                   IS 'Comments balanced by ncloc + comment lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.publicDocumentedApiDensity                            IS 'Public documented classes and functions balanced by ncloc  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.publicUndocumentedApi                                 IS 'Public undocumented classes, functions and variables  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.commentedOutCodeLines                                 IS 'Commented lines of code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.complexity                                            IS 'Cyclomatic complexity  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.fileComplexity                                        IS 'Complexity average by file  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.complexityInClasses                                   IS 'Cyclomatic complexity in classes  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.classComplexity                                       IS 'Complexity average by class  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.complexityInFunctions                                 IS 'Cyclomatic complexity in functions  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.functionComplexity                                    IS 'Complexity average by function  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.classComplexityDistribution                           IS 'Classes distribution /complexity  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.functionComplexityDistributio                         IS 'Functions distribution /complexity  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.fileComplexityDistribution                            IS 'Files distribution /complexity  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.cognitiveComplexity                                   IS 'Cognitive complexity  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.tests                                                 IS 'Number of unit tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.testExecutionTime                                     IS 'Execution duration of unit tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.testErrors                                            IS 'Number of unit test errors  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.skippedTests                                          IS 'Number of skipped unit tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.testFailures                                          IS 'Number of unit test failures  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.testSuccessDensity                                    IS 'Density of successful unit tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.testData                                              IS 'Unit tests details  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.Coverage                                              IS 'Coverage by tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newCoverage                                           IS 'Coverage of new/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.linesToCover                                          IS 'Lines to cover  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newLinesToCover                                       IS 'Lines to cover on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.uncoveredLines                                        IS 'Uncovered lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newUncoveredLines                                     IS 'Uncovered lines on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.lineCoverage                                          IS 'Line coverage  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newLineCoverage                                       IS 'Line coverage of added/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.coverageLineHitsData                                  IS 'Coverage hits by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.conditionsToCover                                     IS 'Conditions to cover  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newConditionsToCover                                  IS 'Conditions to cover on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.uncoveredConditions                                   IS 'Uncovered conditions  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newUncoveredConditions                                IS 'Uncovered conditions on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.branchCoverage                                        IS 'Condition coverage  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newBranchCoverage                                     IS 'Condition coverage of new/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.conditionsByLine                                      IS 'Conditions by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.coveredConditionsByLine                               IS 'Covered conditions by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itCoverage                                            IS 'Integration tests coverage  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItCoverage                                         IS 'Integration tests coverage of new/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itLinesToCover                                        IS 'Lines to cover by Integration Tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItLinesToCover                                     IS 'Lines to cover on new code by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itUncoveredLines                                      IS 'Uncovered lines by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItUncoveredLines                                   IS 'New lines that are not covered by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itLineCoverage                                        IS 'Line coverage by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItLineCoverage                                     IS 'Integration tests line coverage of added/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itCoverageLineHitsData                                IS 'Coverage hits by line by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itConditionsToCover                                   IS 'Integration Tests conditions to cover  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItConditionsToCover                                IS 'Branches to cover by Integration Tests on New Code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itUncoveredConditions                                 IS 'Uncovered conditions by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItUncoveredConditions                              IS 'New conditions that are not covered by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itBranchCoverage                                      IS 'Condition coverage by integration tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newItBranchCoverage                                   IS 'Integration tests condition coverage of new/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itConditionsByLine                                    IS 'IT conditions by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.itCoveredConditionsByLine                             IS 'IT covered conditions by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallCoverage                                       IS 'Overall test coverage  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallCoverage                                    IS 'Overall coverage of new/changed code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallLinesToCover                                   IS 'Overall lines to cover by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallLinesToCover                                IS 'New lines to cover by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallUncoveredLines                                 IS 'Uncovered lines by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallUncoveredLines                              IS 'New lines that are not covered by any tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallLineCoverage                                   IS 'Line coverage by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallLineCoverage                                IS 'Line coverage of added/changed code by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallCoverageLineHitsData                           IS 'Coverage hits by all tests and by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallConditionsToCover                              IS 'Branches to cover by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallConditionsToCover                           IS 'New branches to cover by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallUncoveredConditions                            IS 'Uncovered conditions by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallUncoveredCondition                          IS 'New conditions that are not covered by any test  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallBranchCoverage                                 IS 'Condition coverage by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newOverallBranchCoverage                              IS 'Condition coverage of new/changed code by all tests  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallConditionsByLine                               IS 'Overall conditions by all tests and by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.overallCoveredConditionsByL                           IS 'Overall covered conditions by all tests and by line  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.duplicatedLines                                       IS 'Duplicated lines  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newDuplicatedLines                                    IS 'Duplicated Lines on New Code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.duplicatedBlocks                                      IS 'Duplicated blocks  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newDuplicatedBlocks                                   IS 'Duplicated blocks on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.duplicatedFiles                                       IS 'Duplicated files  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.duplicatedLinesDensity                                IS 'Duplicated lines balanced by statements  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newDuplicatedLinesDensity                             IS 'Duplicated lines on new code balanced by statements  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.duplicationsData                                      IS 'Duplications details  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.Violations                                            IS 'Issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.blockerViolations                                     IS 'Blocker issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.criticalViolations                                    IS 'Critical issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.majorViolations                                       IS 'Major issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.minorViolations                                       IS 'Minor issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.infoViolations                                        IS 'Info issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newViolations                                         IS 'New issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newBlockerViolations                                  IS 'New Blocker issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newCriticalViolations                                 IS 'New Critical issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newMajorViolations                                    IS 'New Major issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newMinorViolations                                    IS 'New Minor issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newInfoViolations                                     IS 'New Info issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.falsePositiveIssues                                   IS 'False positive issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.wontFixIssues                                         IS 'Wont fix issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.openIssues                                            IS 'Open issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.reopenedIssues                                        IS 'Reopened issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.confirmedIssues                                       IS 'Confirmed issues  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.codeSmells                                            IS 'Code Smells  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newCodeSmells                                         IS 'New Code Smells  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.bugs                                                  IS 'Bugs  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newBugs                                               IS 'New Bugs  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.Vulnerabilities                                       IS 'Vulnerabilities  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newVulnerabilities                                    IS 'New Vulnerabilities  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.sqaleIndex                                            IS 'Total effort (in days) to fix all the issues on the component and therefore to comply to all the requirements.  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newTechnicalDebt                                      IS 'Added technical debt  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.sqaleRating                                           IS 'A-to-E rating based on the technical debt ratio  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newMaintainabilityRating                              IS 'Maintainability rating on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.developmentCost                                       IS 'Development cost  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newDevelopmentCost                                    IS 'Development cost on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.sqaleDebtRatio                                        IS 'Ratio of the actual technical debt compared to the estimated cost to develop the whole source code from scratch ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newSqaleDebtRatio                                     IS 'Technical Debt Ratio of new/changed code.  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.effortToReachMaintainability                          IS 'Effort to reach maintainability rating A  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.reliabilityRemediationEffort                          IS 'Reliability Remediation Effort  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newReliabilityRemediationEff                          IS 'Reliability remediation effort on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.reliabilityRating                                     IS 'Reliability rating  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newReliabilityRating                                  IS 'Reliability rating on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.securityRemediationEffort                             IS 'Security remediation effort  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newSecurityRemediationEffort                          IS 'Security remediation effort on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.securityRating                                        IS 'Security rating  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.newSecurityRating                                     IS 'Security rating on new code  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.nclocData                                             IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.commentLinesData                                      IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.executableLinesData                                   IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.alertStatus                                           IS 'The project status with regard to its quality gate.  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.qualityGateDetails                                    IS 'The project detailed status with regard to its quality gate  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.qualityProfiles                                       IS 'Details of quality profiles used during analysis  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.lastCommitDate                                        IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.burnedBudget                                          IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.businessValue                                         IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.teamSize                                              IS 'NULL  ';
COMMENT
ON COLUMN public.gov_sonar_metrics.sonarjavaFeedback                                     IS 'NULL  ';


