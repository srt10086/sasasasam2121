package com.cv.service;

import com.cv.annotions.ATest;
import org.springframework.stereotype.Service;

@Service("service2")
public class S2 {
    @ATest("123")
    public void ss() {
        System.out.println("执行了方法");
    }
    @ATest("456")
    public void ss1() {
        System.out.println("执行了方法");
    }
}
