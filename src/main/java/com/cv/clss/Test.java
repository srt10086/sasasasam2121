package com.cv.clss;

import cn.hutool.core.io.StreamProgress;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class Test {

    @AT("name")
    public void test(String s, String sss) {
        System.out.println("");
    }

    public static void main(String[] args) {
        Set<Class<?>> classes = ClassUtil.scanPackage("com.cv");
//        try {
//            JarFile jf = new JarFile("E:\\maven\\dk\\brics\\automaton\\automaton\\1.11-8\\automaton-1.11-8.jar");
//            Enumeration<JarEntry> entries = jf.entries();
//            while (entries.hasMoreElements()) {
//                ZipEntry zipEntry = entries.nextElement();
//                String name = zipEntry.getName();
//                if (name.contains("StatePair")) {
//                    System.out.println();
//                    InputStream inputStream = jf.getInputStream(zipEntry);
//                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//                    String str;
//                    while ((str = bufferedReader.readLine()) != null) {
//                        System.out.println(str);
//                    }
//                    System.out.println(name);
//                }
//            }
//            System.out.println();
//            Enumeration<URL> resources = ClassUtil.getContextClassLoader().getResources("ribbon-loadbalancer.properties");
//            System.out.println();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        for (Class<?> aClass : classes) {
            Method[] methods = aClass.getMethods();
            for (int i = 0; i < methods.length; i++) {
                Method method = methods[i];
                AT annotation = method.getAnnotation(AT.class);
                if (null != annotation) {
                    try {
                        Parameter[] parameters = method.getParameters();
                        Object[] params = mappingParams(null, parameters);
                        method.invoke(aClass.newInstance(), params);
                        break;
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e.getMessage());
                    } catch (InvocationTargetException e) {
                        throw new RuntimeException(e.getMessage());
                    } catch (InstantiationException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                }
            }

            System.out.println();
        }
//        File file = null;
//        Package aPackage = Test.class.getPackage();
//        String packageDirName = aPackage.getName().replace('.', '/');
//        try {
//            Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
//            URL url = resources.nextElement();
//            String protocol = url.getProtocol();
//            if (protocol.equals("file")) {
//                String filePath = url.getFile();
//                System.out.println(filePath);
//                filePath = filePath.substring(1) + "/xxxx.jar";
//                file = new File(filePath);
//                System.out.println();
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        HttpUtil.downloadFile("https://repo1.maven.org/maven2/com/armanbilge/schrodinger-kernel_3/0.3-203-59c7ace/schrodinger-kernel_3-0.3-203-59c7ace.jar", file, new StreamProgress() {
//            @Override
//            public void start() {
//                System.out.println("开始下载");
//            }
//
//            @Override
//            public void progress(long l) {
//                System.out.println("进度：" + l);
//            }
//
//            @Override
//            public void finish() {
//                System.out.println("下载完成");
//            }
//        });
//        file.delete();

    }

    private static Object[] mappingParams(Map<String, Object> params, Parameter[] parameters) {
        Object[] paramObject = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (!ObjectUtil.isEmpty(params)) {
                String name = parameters[i].getName();
                paramObject[i] = params.get(name);
            }
        }
        return paramObject;
    }
}
