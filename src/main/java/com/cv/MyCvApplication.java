package com.cv;

import com.cv.annotions.ATest;
import com.cv.test.AnnotationsUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootApplication
public class MyCvApplication {

    public static void main(String[] args) {
        Runtime.getRuntime().gc();
        SpringApplication.run(MyCvApplication.class, args);
        ReentrantLock lock = new ReentrantLock();
        List<Class<?>> list = AnnotationsUtil.getClassesWithAnnotationFromPackage(MyCvApplication.class.getPackage().getName(), Service.class);
        list.forEach(aClass -> {
            Method[] declaredMethods = aClass.getDeclaredMethods();
            for (int i = 0; i < declaredMethods.length; i++) {
                ATest aTest = declaredMethods[i].getAnnotation(ATest.class);
                if (null != aTest) {
                    try {
                        Object invoke = declaredMethods[i].invoke(aClass.newInstance(), args);
                        System.out.println("");
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }
                    System.out.println("assa"+aTest.value());
                }
            }
            System.out.println();
        });

        System.out.println();

    }

}
