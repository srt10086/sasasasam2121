package com.cv.netty;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DemoTest {

    public static void main(String[] args) {
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
                2,
                4,
                100, TimeUnit.MINUTES,
                new LinkedBlockingDeque<>());
        poolExecutor.submit(new Runnable() {
            @Override
            public void run() {

            }
        });
    }
}
