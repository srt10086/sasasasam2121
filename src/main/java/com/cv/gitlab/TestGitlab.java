package com.cv.gitlab;


import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.ProjectApi;
import org.gitlab4j.api.models.*;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestGitlab {
//    public static void main(String[] args) throws Exception {
////        get();
////        getUser();
//        String url = "https://gitlab.com";
//        String userName = "shisheng@live.com";
////        String privateToken = "NYrC5uNXrHHjpdKBKK3z";
//
//        String privateToken = "KyuyQxkVe9J6Kc5wwe6x";
////        createProject(url, privateToken);
//
////        createBranch(url, privateToken);
//
////        delBranch(url, privateToken);
//
////        mergeBranch(url, privateToken);
//        String projectName = "sasasasam2121";
//        GitLabApi gitLabApi = new GitLabApi(url, privateToken);
//        List<Project> projectList = gitLabApi.getProjectApi().getProjects(projectName);
//        Map<String, List<Map<String, String>>> logs = null;
//        for (Project project : projectList) {
//            if (projectName.equals(project.getName())) {
//                logs = getCommitLog(gitLabApi, project, "2021-10-18 10:45:12", "dev");
//                System.out.println("sasa");
//                break;
//            }
//        }
//        System.out.println();
//    }

    /**
     * 删除分支信息
     */
    private static void delBranch(String url, String privateToken) throws GitLabApiException {
        GitLabApi gitLabApi = new GitLabApi(url, privateToken);
        Project project = gitLabApi.getProjectApi().getProject(30565864);
        gitLabApi.getRepositoryApi().deleteBranch(project.getId(), "dev");
        System.out.println();
    }

    /**
     * 合并分支
     **/
    private static void mergeBranch(String url, String privateToken) throws GitLabApiException {
        GitLabApi gitLabApi = new GitLabApi(url, privateToken);
        Project project = gitLabApi.getProjectApi().getProject(30565864);
        MergeRequestParams params = new MergeRequestParams()
                .withRemoveSourceBranch(false)
                .withAssigneeId(null)
                .withTitle("dev into main")
                .withDescription("dev into main")
                .withSourceBranch("dev")
                .withTargetBranch("dev1");
        MergeRequest mergeRequest1 = gitLabApi.getMergeRequestApi().createMergeRequest(project.getId(), params);
        gitLabApi.getMergeRequestApi().acceptMergeRequest(project.getId(), mergeRequest1.getIid());
        System.out.println();
    }

    /**
     * 创建分支
     */
    private static void createBranch(String url, String privateToken) throws GitLabApiException {
        GitLabApi gitLabApi = new GitLabApi(url, privateToken);
        Project project = gitLabApi.getProjectApi().getProject(30565864);
        Branch dev = gitLabApi.getRepositoryApi().createBranch(project.getId(), "dev1", "main");
        System.out.println();
    }

    /**
     * 创建项目
     */
    private static void createProject(String url, String privateToken) throws GitLabApiException {
        GitLabApi gitLabApi = new GitLabApi(url, privateToken);
        Project project = new Project()
                .withName("sasasasam212111")
                .withDescription("GitLab4J test project.")
                .withIssuesEnabled(true)
                .withMergeRequestsEnabled(true)
                .withWikiEnabled(true)
                .withSnippetsEnabled(true)
                .withVisibility(Visibility.PRIVATE);
        Project myProject = gitLabApi.getProjectApi().createProject(project);
        System.out.println();
    }

    private static Map<String, List<Map<String, String>>> getCommitLog(GitLabApi gitLabApi, Project project, String lastTime, String branchName) {
        Map<String, List<Map<String, String>>> res = new HashMap<>();
        List<Map<String, String>> commitLogs = new ArrayList<>();
        List<Map<String, String>> commitFiles = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Commit> commits = null;
        try {
            commits = gitLabApi.getCommitsApi().getCommits(project.getId(), branchName, sdf.parse(lastTime), new Date());
        } catch (GitLabApiException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (Commit commit : commits) {
            Map<String, String> commitLog = new HashMap<>();
            commitLog.put("revision", commit.getId());
            commitLog.put("author", commit.getAuthorName());
            commitLog.put("cmtDate", sdf.format(commit.getCommittedDate()));
            commitLog.put("message", commit.getMessage());
            Set<String> actions = new HashSet<>();
            List<String> parentIds = commit.getParentIds();
            List<Diff> diffList = null;
            try {
                diffList = gitLabApi.getCommitsApi().getDiff(project.getId(), commit.getId());
            } catch (GitLabApiException e) {
                e.printStackTrace();
            }
            List<Map<String, String>> changes = getChangesFromDiffs(diffList, actions, commit.getId());
            commitLog.put("actions", actions.toString());
            commitLogs.add(commitLog);
            commitFiles.addAll(changes);
        }
        res.put("commitLogs", commitLogs);
        res.put("commitFiles", commitFiles);
        gitLabApi.close();
        return res;
    }

    private static List<Map<String, String>> getChangesFromDiffs(List<Diff> diffList, Set<String> actions, String commitId) {
        List<Map<String, String>> changes = new ArrayList<>();
        for (Diff diff : diffList) {
            Map<String, String> change = new HashMap<>();
            String actType = diff.getDeletedFile() ? "D" : (diff.getNewFile() ? "A" : "M");
            String filePtah = diff.getOldPath();
            change.put("actType", actType);
            change.put("filePath", filePtah);
            change.put("revision", commitId);
            changes.add(change);
        }
        return changes;
    }

    private void gitLab(){
        try {
            GitLabApi gitLabApi = GitLabApi.oauth2Login("https://gitlab.com", "srt10086", "a1039385286");
            ProjectApi projectApi = gitLabApi.getProjectApi();
            RepositoryFile main = gitLabApi.getRepositoryFileApi().getFile("30565864", "README.md", "main");
            System.out.println();
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        TestGitlab testGitlab = new TestGitlab();
        testGitlab.gitLab();
    }
}

