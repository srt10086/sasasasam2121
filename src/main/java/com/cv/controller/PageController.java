package com.cv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Controller
@RequestMapping("/page")
public class PageController {
    /**
     * 测试页面渲染
     */
    @RequestMapping("/test")
    public ModelAndView testPage(ModelAndView mv) {
        Lock lock = new ReentrantLock();


        //简历、数据打包下载
        mv.setViewName("/cv");
        mv.addObject("url","http://baidu.com");
        return mv;
    }

}
