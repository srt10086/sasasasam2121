package com.cv.controller;

import com.cv.util.MultipartFileToFile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * 简历管理
 */
@RestController
@RequestMapping("/cv")
public class CvController {

    /**
     * 上传简历信息
     */
    @PostMapping("/upload")
    public Object uploadCV(@RequestParam("file") MultipartFile file) throws Exception {
        System.out.println(file.getName());
        File file1 = MultipartFileToFile.multipartFileToFile(file);
        File f = new File("E:/IdeaProjects/myCv/src/main/resources/"+file1.getName());
        OutputStream out = new FileOutputStream(f);
        InputStream in = new FileInputStream(file1);
        byte[] bytes = new byte[1024];
        while (in.read(bytes)!=-1){
            out.write(bytes);
        }
        out.close();
        in.close();
        return "上传信息";
    }

    /**
     * 下载简历信息
     */
    @RequestMapping("/download")
    public Object download(String download) {
        //简历、数据打包下载
        return "下载信息";
    }

}
