package com.cv.leetCode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution15 {

    //a+b+c = 0
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        int length = nums.length;
        if (length<3){
            return list;
        }
        List<Integer> list1 = new ArrayList<>();
        int indexA = 0;
        int indexB = 0;
        int indexC = 0;
        for (int i = 0; i <length&&indexA!=indexB&&indexB!=indexC&&indexA!=indexB; i++) {
            Integer a = nums[i];
            for (; indexA < length; indexA++) {
                Integer b = nums[indexA];
                for (; indexB < length; indexB++) {
                    Integer c = nums[indexC];
                    if ((a+b+c)==0){
                        System.out.println(""+a+""+"+"+b+"+"+c);
                        break;
                    }
                }
                break;
            }
        }

        return list;
    }

    public static void main(String[] args) {
        Solution15 solution15 =  new Solution15();
        int[] integers = new int[]{-1,0,1,2,-1,-4};
        solution15.threeSum(integers);
    }
}
