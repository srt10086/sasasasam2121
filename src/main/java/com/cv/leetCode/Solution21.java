package com.cv.leetCode;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Solution21 {

    private ReentrantLock reentrantLock = new ReentrantLock();


    public ListNode mergeTwoLists(int thNum) throws InterruptedException {
        reentrantLock.lock();
        try {
            System.out.println("加锁:" + thNum);
            TimeUnit.SECONDS.sleep(1);
        } finally {
            System.out.println("释放锁：" + thNum);
            reentrantLock.unlock();
        }

        return null;
    }

    public static void main(String[] args) throws InterruptedException {
        Solution21 solution21 = new Solution21();
        for (int i = 0; i < 20; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        solution21.mergeTwoLists(finalI);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }


    }
}
