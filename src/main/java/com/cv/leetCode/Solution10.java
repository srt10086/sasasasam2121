package com.cv.leetCode;


import java.util.HashMap;
import java.util.Map;

public class Solution10 {

    public int[] getArr(int[] arr, int target) {
        Map<Integer,Integer> map = new HashMap();
        int[] temp = new int[2];
        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(target-arr[i])){
                temp[0] = map.get(target-arr[i]);
                temp[1] = map.get(target-arr[i]);
                return temp;
            }
            map.put(arr[i],i);
        }
        return temp;
    }

}
