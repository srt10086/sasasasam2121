package com.cv.leetCode;

import java.math.BigInteger;
import java.util.List;

public class Solution6 {


    public String convert(String s, int numRows) {
        int length = s.length();
        //有几个hen
        int width = length % numRows + length / numRows;
        //存放盒子的东西
        String[][] box = new String[width][numRows];
        //  M = (h -2)*( h -1)
        List<String> list = getItem(s, numRows);
        return null;
    }

    private List<String> getItem(String str, int numRows) {
        int index = 0;
        //公式：M = (h -2)*( h -1)
        return null;
    }

    public int reverse(int x) {
        StringBuilder sbs = new StringBuilder(String.valueOf(x));
        StringBuilder reverse = sbs.reverse();
        String s = reverse.toString();
        char[] chars = s.toCharArray();
        int length = chars.length;
        String str;
        boolean   flag =false;
        if (length>1){
            if (chars[length - 1] == '-') {
                str= s.substring(0,length-1);
                flag = true;
            }else {
                str  = s;
            }
            if (chars[0]=='0'){
                str = str.substring(1);
            }
        }else {
            str = s;
        }
        Integer integer = Integer.parseInt(str.trim());
        return flag?-integer:integer;
    }
    public int reverseq(int x) {
        int res = 0;
        while(x!=0) {
            //每次取末尾数字
            int tmp = x%10;
            //判断是否 大于 最大32位整数
            if (res>214748364 || (res==214748364 && tmp>7)) {
                return 0;
            }
            //判断是否 小于 最小32位整数
            if (res<-214748364 || (res==-214748364 && tmp<-8)) {
                return 0;
            }
            res = res*10 + tmp;
            x /= 10;
        }
        return res;
    }
    public static void main(String[] args) {
        Solution6 solution6 = new Solution6();
        solution6.reverseq(-102393892);
    }
}
