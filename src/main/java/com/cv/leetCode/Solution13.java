package com.cv.leetCode;


import java.util.*;

class   Solution {
    Stack<Character> characterStack = new Stack<>();

    public boolean isValid(String s) {
        //声明一个map存放匹配关系
        HashMap<Character,Character> map = new HashMap<>();
        map.put('(',')');
        map.put('{','}');
        map.put('[',']');
        //声明一个链表结构的栈
        LinkedList<Character> stack = new LinkedList<>();
        for(int i = 0;i<s.length();i++){
            char cur = s.charAt(i);
            //入栈的四种情况
            if(cur=='(' || cur=='[' || cur=='{' || stack.size()==0 ){
                stack.push(cur);
            }else {
                //否则取栈顶元素出栈，判断是否与当前字符匹配
                char tmp = stack.pop();
                //如果栈顶元素为右括号，则以其为键取映射必为空，此时直接返回false
                if(map.get(tmp)==null){
                    return false;
                }
                //判断当前右括号与栈里的左括号是否匹配，不匹配直接返回false
                if(map.get(tmp)!=cur){
                    return false;
                }
            }
        }
        //循环结束，栈元素不为空，说明有不匹配的，返回false
        if(stack.size()!=0){
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        Solution sol = new Solution();
        System.out.println( sol.isValid(""));
    }
}
