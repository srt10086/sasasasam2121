package com.cv.leetCode;

public class SolutionDST {
    //栈实现队列
    private SolutionSTD std = new SolutionSTD();
    private SolutionSTD std1 = new SolutionSTD();
    private int size = 0;
    private int popIndex = 0;

    public void push(int node) {
        size++;
        std.push(node);

    }

    public Integer pop() {
        Integer[] obs = new Integer[size];
        int index = 0;
        while (!std.isEmpty()) {
            obs[index] = std.pop();
            index++;
        }
        popIndex = size;
        return obs[--popIndex];
    }

    public static void main(String[] args) {
        //先进后出
        SolutionDST dst = new SolutionDST();
        dst.push(1);
        dst.push(2);
        dst.push(3);
        System.out.println("sasasa");
        System.out.println(dst.pop());
        System.out.println(dst.pop());
        System.out.println(dst.pop());
    }
}
