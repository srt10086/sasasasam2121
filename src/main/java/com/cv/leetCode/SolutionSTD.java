package com.cv.leetCode;

import java.util.Stack;

public class SolutionSTD {
    private Stack<Integer> stack1 = new Stack<Integer>();
    private Stack<Integer> stack2 = new Stack<Integer>();
    private int size = 0;

    public void push(int node) {
        stack1.push(node);
        size++;
    }

    public int pop() {
        while (!stack1.empty()) {
            stack2.push(stack1.pop());
        }
        --size;
        return stack2.pop();
    }

    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        SolutionSTD solutionSD = new SolutionSTD();
        solutionSD.push(1);
        solutionSD.push(2);
        solutionSD.push(3);
        System.out.println("sasasa");
        System.out.println("顺序：" + solutionSD.pop());
        System.out.println("顺序：" + solutionSD.pop());
        System.out.println("顺序：" + solutionSD.pop());
    }
}
