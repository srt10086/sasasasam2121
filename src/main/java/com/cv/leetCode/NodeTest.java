package com.cv.leetCode;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class NodeTest {

    public static class Node {
        public int value;
        public Node next;

        public Node(int data, Node node) {
            this.value = data;
            this.next = node;
        }
    }

    public Node reverse(Node head) {
        if (head == null || head.next == null)
            return head;
        Node temp = head.next;
        Node newHead = reverse(head.next);
        temp.next = head;
        head.next = null;
        return newHead;
    }

    public static Node reverseList(Node node) {
        Node pre = null;
        Node temp = null;
        while (node != null) {
            //1
            temp = node.next;
            //null
            node.next = pre;
            //取出一个值
            pre = node;
            //放下一个节点
            node = temp;
        }
        return pre;
    }

    public static void main(String[] args) {
        Node node = new Node(5, null);
        Node node1 = new Node(4, node);
        Node node2 = new Node(3, node1);
        Node node3 = new Node(2, node2);
        Node node4 = new Node(1, node3);
        Node node5 = reverseList(node4);
        System.out.println();
        ReentrantLock lock = new ReentrantLock();
        //加锁
        try {
            lock.lock();
        } finally {
            lock.unlock();
        }
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(1,2,200, TimeUnit.MINUTES,new LinkedBlockingDeque<>());
        poolExecutor.submit(new Runnable() {
            @Override
            public void run() {

            }
        });

        poolExecutor.shutdown();
        poolExecutor.shutdownNow();
        ReentrantReadWriteLock lock1 = new ReentrantReadWriteLock();
        lock1.readLock();
        lock1.writeLock();

    }
}