package com.cv.leetCode;

import java.util.HashMap;

public class Solution3 {

    public int lengthOfLongestSubstring(String str) {
        HashMap<Character, Integer> window = new HashMap<>();
        int max = 0;
        int left = 0;
        for (int i = 0; i < str.length(); i++) {
            if (window.containsKey(str.charAt(i))) {
                max = window.size();
                System.out.println();
            } else {
                window.put(str.charAt(i), i);
                max = i;
            }
        }

        return max;
    }

    public static void main(String[] args) {
        Solution3 solution3 = new Solution3();
        solution3.lengthOfLongestSubstring("abdcabcasbcbd");
        HashMap map =  new HashMap();

    }
}
