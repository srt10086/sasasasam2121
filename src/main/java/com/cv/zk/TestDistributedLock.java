package com.cv.zk;


import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class TestDistributedLock {
    public static void main(String[] args) throws IOException {
        //模拟10个线程去访问锁，只有一个线程获得锁。
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                DistributeLock lock = new DistributeLock();
                try {
                    //先阻塞
                    countDownLatch.await();
                    lock.lock();//获得锁

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
//                    lock.unlock();
                }
            }
                    , "Thread-" + i).start();
            countDownLatch.countDown();
        }
        //键盘输入。
        System.in.read();
    }



}

