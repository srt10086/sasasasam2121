package com.cv.zk;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Async {
    public static void main(String[] args) throws SQLException {
//        CompletableFuture future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("第一个步骤");
//            return "调用完成结果";
//        }).thenCompose(s -> CompletableFuture.supplyAsync(() -> {
//            System.out.println("第二个步骤" + s);
//            return "调用2";
//        }));
        //异步任务编排
//        CompletableFuture.supplyAsync(() -> {
//            return "并行计算1";
//        }).thenCombine(CompletableFuture.supplyAsync(() -> {
//            return "并行计算2";
//        }), (s, s1) -> {
//            System.out.println(s + "计算机会" + s1);
//            return s + "计算机会" + s1;
//        });
        PsqlConnectionTool pgtool = new PsqlConnectionTool();
        Connection conn = pgtool.getConn();
        ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM test");
        System.out.println(resultSet);
    }
}
